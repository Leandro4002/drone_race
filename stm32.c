#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define DEBUG_MODULE "FOLLOW_LINE"
#include "app.h"
#include "FreeRTOS.h"
#include "task.h"
#include "debug.h"
#include "commander.h"
#include "pm.h"
#include "log.h"
#include "cpx_internal_router.h"

#define FLY_HEIGHT 0.2

uint8_t drone_command = 0;

void get_drone_command(const CPXPacket_t* cpxRx) {
   drone_command = cpxRx->data[0];
}

static void setAbsoluteSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate) {
   setpoint->mode.z = modeAbs;
   setpoint->position.z = z;
   setpoint->mode.yaw = modeVelocity;
   setpoint->attitudeRate.yaw = yawrate;
   setpoint->mode.x = modeAbs;
   setpoint->mode.y = modeAbs;
   setpoint->velocity.x = vx;
   setpoint->velocity.y = vy;
   setpoint->velocity_body = true;
}

void appMain() {
   //cpxRegisterAppMessageHandler(get_drone_command);

   // Wait for GAP8 init to be done so we don't miss packets
   vTaskDelay(M2T(2000));

   setpoint_t current_setpoint;

   while (1) {
      switch (drone_command) {
         case 0:
            setAbsoluteSetpoint(&current_setpoint, 0, 0, FLY_HEIGHT, 0);
            commanderSetSetpoint(&current_setpoint, 3);
            break;
         // TODO handle all others commands (go forward, go left, go right, etc.)
         default:
            // Commande inconnue!
            return;
      }

      vTaskDelay(M2T(100));
   }
}

