# Python script to be used inside Blender 3D modeling program (version 4.1.1)
# Assets imported using the Blenderkit addon
# It randomly moves, twists and/or make some objects disappear. It also moves around the white line.
# In real life, parkour is about 3.5m x 1.8m

# Simple commande line to inject a script into blender's python console
# exec(compile(open('/your/path/randomize.py').read(), '/your/path/randomize.py', 'exec'))

from mathutils import Euler
import random
import math
import time

save_path_render = "/home/leandro/git/drone_race/render"
save_file_labels = "/home/leandro/git/drone_race/labels.json"
save_file_labels_csv = "/home/leandro/git/drone_race/labels.csv"
drift_force = 0.10
bottle_spwan_region_width_x = 0.3
bottle_spwan_region_width_y = 0.2
parkour_size_x = 1.9
parkour_size_y = 1
does_render = True
nb_iteration = 20
rand_cam_x = 0.05
rand_cam_y = 0.05

def wrap_to_pi(value):
    """
    Wrap a given angle value to the range [-π, π].
    
    Parameters:
        value (float): The angle in radians to be wrapped.
        
    Returns:
        float: The angle wrapped to the range [-π, π].
    """
    # Use the modulo operation to wrap the value within the range [-2π, 2π]
    value = (value + math.pi) % (2 * math.pi) - math.pi
    
    return value

def generate_random_coordinate(outer_x_min, outer_x_max, outer_y_min, outer_y_max, 
                               inner_x_min, inner_x_max, inner_y_min, inner_y_max):
    while True:
        x = random.uniform(outer_x_min, outer_x_max)
        y = random.uniform(outer_y_min, outer_y_max)
        
        if not (inner_x_min <= x <= inner_x_max and inner_y_min <= y <= inner_y_max):
            return x, y

json_data = "["
csv_data = ""

for iteration in range(nb_iteration):
    bottle_density = random.uniform(0.5, 0.8)
    nb_bottles = 0
    angles = []
    # Set bottles positions
    for j in range(10):
        cur_bottle = "bottle" + str(j)

        if random.uniform(0, 1.0) > bottle_density:
            bpy.data.objects[cur_bottle].location.z = -1
            continue
        
        bpy.data.objects[cur_bottle].location.z = 0
        nb_bottles += 1

        # Generate bottle position on the outside border
        bpy.data.objects[cur_bottle].location.x, bpy.data.objects[cur_bottle].location.y = generate_random_coordinate(-parkour_size_x, parkour_size_x, -parkour_size_y, parkour_size_y, -parkour_size_x + bottle_spwan_region_width_x, parkour_size_x - bottle_spwan_region_width_x, -parkour_size_y + bottle_spwan_region_width_y, parkour_size_y - bottle_spwan_region_width_y)

    # Set line position
    line_v = bpy.data.objects["line"].data.splines[0].bezier_points
    x_rand = 0
    y_rand = 0
    for i in range(len(line_v) - 1):

        x_rand += random.uniform(-drift_force, drift_force)
        y_rand += random.uniform(-drift_force, drift_force)

        # Bring back random to 0, so it comes back smoothly at the begining
        back_drift = 1.1 - (i + 1) / len(line_v)
        x_rand *= back_drift
        y_rand *= back_drift

        scaled_i = i / (len(line_v) - 1) * 2 * math.pi
        line_v[i].co.x = math.sin(scaled_i) * 1.1 + x_rand
        line_v[i].co.y = math.cos(scaled_i) * 0.5 + y_rand
        line_v[i].handle_left = line_v[i].co
        line_v[i].handle_right = line_v[i].co
        if (i == 0):
            line_v[0].handle_left.x = -0.1
            line_v[0].handle_right.x = 0.1

    # Join last vertex to start vertex
    line_v[len(line_v) - 1].co = line_v[0].co
    line_v[len(line_v) - 1].handle_left = line_v[0].co
    line_v[len(line_v) - 1].handle_right = line_v[0].co

    direction = 1 if ((iteration % 2) == 0) else -1

    for i in range(len(line_v) - 1):
        # Move camera on vertex
        bpy.data.objects["camera"].location.x = line_v[i].co.x + random.uniform(-rand_cam_x, rand_cam_x)
        bpy.data.objects["camera"].location.y = line_v[i].co.y + random.uniform(-rand_cam_y, rand_cam_y)

        # shorthands
        camx = bpy.data.objects["camera"].location.x
        camy = bpy.data.objects["camera"].location.y

        # Camera angle
        avg_cam_x = 0
        avg_cam_y = 0
        for j in range(4):
            avg_cam_x += line_v[(i+(j+1)*direction)%len(line_v)].co.x
            avg_cam_y += line_v[(i+(j+1)*direction)%len(line_v)].co.y
        avg_cam_x /= 4
        avg_cam_y /= 4
        delta_cam_x = avg_cam_x - camx
        delta_cam_y = avg_cam_y - camy
        camera_angle_global = math.atan2(delta_cam_y, delta_cam_x)
        bpy.data.objects["camera"].rotation_euler[2] = camera_angle_global - math.radians(90)
        
        # Label angle (dot product between camera angle and a further point on the line)
        avg_lbl_x = 0
        avg_lbl_y = 0
        for j in range(8):
            avg_lbl_x += line_v[(i+(j+1)*direction)%len(line_v)].co.x
            avg_lbl_y += line_v[(i+(j+1)*direction)%len(line_v)].co.y
        avg_lbl_x /= 8
        avg_lbl_y /= 8
        delta_lbl_x = avg_lbl_x - camx
        delta_lbl_y = avg_lbl_y - camy
        angle_global = math.atan2(delta_lbl_y, delta_lbl_x)
        angle_side = 1 if (wrap_to_pi(camera_angle_global) - wrap_to_pi(angle_global) >= 0) else -1
        currAngle = math.acos(
        (delta_cam_x * delta_lbl_x + delta_cam_y * delta_lbl_y) /
        (math.sqrt(delta_cam_x*delta_cam_x + delta_cam_y*delta_cam_y) * math.sqrt(delta_lbl_x*delta_lbl_x + delta_lbl_y*delta_lbl_y))
        ) * angle_side
        angles.append(currAngle)

        file_num = i if direction == 1 else (len(line_v) - 2 - i)

        csv_data += str(iteration).zfill(2)+"_"+str(file_num).zfill(2)+".png"+";"+str(currAngle)+"\n"
        
        if does_render:

            bpy.context.scene.render.filepath = str(save_path_render) + "/" + str(iteration).zfill(2) +"_"+ str(file_num).zfill(2) + ".png"
            bpy.context.scene.render.image_settings.file_format = 'PNG'
            bpy.context.scene.render.image_settings.color_mode = 'BW'
            bpy.ops.render.render(write_still=True)

    if direction == -1:
        angles.reverse()
    
    json_data += "{\"nb_bottles\":"+str(nb_bottles)+",\"angles\":"+str(angles)+"}"
    if (iteration != nb_iteration -1):
        json_data += ","

json_data += "]"

with open(save_file_labels, 'w') as file:
    file.write(json_data)

with open(save_file_labels_csv, 'w') as file:
    file.write(csv_data)

if does_render:
    print("Rendered "+str(nb_iteration * (len(line_v) - 1))+" images in " + str(save_path_render))

