#include "pmsis.h"
#include "cpx.h"
#include "wifi.h"
#include "bsp/bsp.h"
#include "bsp/camera/himax.h"
#include "bsp/buffer.h"

#define CAMERA_WIDTH 324
#define CAMERA_HEIGHT 244
#define IMG_ORIENTATION 0x0101

int open_pi_camera_himax(struct pi_device *device);
static void camera_control_ok(void *arg);
void camera_task(void *parameters);

static struct pi_device camera;
unsigned char *imageBuffer;
static pi_buffer_t buffer;
static SemaphoreHandle_t capture_sem = NULL;

typedef struct {
   size_t width;
   size_t height;
   unsigned char *data;
} img_t;

uint8_t droneCommand = 0;

void start(void) {
   cpxInit();
   cpxEnableFunction(CPX_F_WIFI_CTRL);

   // Init stuff
   if (open_pi_camera_himax(&camera)) {
      cpxPrintToConsole(LOG_TO_CRTP, "Cannot open camera!\n");
      return;
   }
   pi_buffer_init(&buffer, PI_BUFFER_TYPE_L2, imageBuffer);
   pi_buffer_set_format(&buffer, CAMERA_WIDTH, CAMERA_HEIGHT, 1, PI_BUFFER_FORMAT_GRAY);
   imageBuffer = (unsigned char*)pmsis_l2_malloc(CAMERA_WIDTH * CAMERA_HEIGHT);

   BaseType_t cameraTask = xTaskCreate(camera_task, "camera_task", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);

   while (1) {
      // TODO control the drone
      droneCommand = 0;

      // Send drone command to stm32
      CPXPacket_t packet;
      cpxInitRoute(CPX_T_GAP8, CPX_T_STM32, CPX_F_APP, &packet.route);
      packet.data[0] = droneCommand;
      packet.dataLength = 1;
      cpxSendPacketBlocking(&packet);

      pi_yield();
      vTaskDelay(100);
   }
}

static void camera_control_ok(void *arg) {
   pi_camera_control(&camera, PI_CAMERA_CMD_STOP, 0);
   xSemaphoreGive(capture_sem);
}

void camera_task(void *parameters) {
   pi_task_t captureDoneTask;
   static pi_buffer_t buffer;
   vTaskDelay(2000);

   // Prepare camera output image buffer
   uint32_t resolution = CAMERA_WIDTH * CAMERA_HEIGHT;
   imageBuffer = (unsigned char *)pmsis_l2_malloc(resolution * sizeof(unsigned char));
   if (imageBuffer == NULL) {
      cpxPrintToConsole(LOG_TO_CRTP, "Cannot malloc image\n");
      return;
   }

   // Init camera for control
   if (open_pi_camera_himax(&camera)) {
      cpxPrintToConsole(LOG_TO_CRTP, "Cannot open camera\n");
      return;
   }

   pi_buffer_init(&buffer, PI_BUFFER_TYPE_L2, imageBuffer);
   pi_buffer_set_format(&buffer, CAMERA_WIDTH, CAMERA_HEIGHT, 1, PI_BUFFER_FORMAT_GRAY);

   // Init semaphore
   capture_sem = xSemaphoreCreateBinary();

   // Set callback when camera is stopped
   pi_camera_capture_async(&camera, imageBuffer, resolution, pi_task_callback(&captureDoneTask, camera_control_ok, NULL));
   // Start taking picture
   pi_camera_control(&camera, PI_CAMERA_CMD_START, 0);
   // Wait on camera is finished taking photo
   xSemaphoreTake(capture_sem, portMAX_DELAY);

   pmsis_l2_malloc_free(imageBuffer, CAMERA_WIDTH * CAMERA_HEIGHT);
}

int open_pi_camera_himax(struct pi_device *device) {
   struct pi_himax_conf cam_conf;

   pi_himax_conf_init(&cam_conf);

   cam_conf.format = PI_CAMERA_QVGA;

   pi_open_from_conf(device, &cam_conf);
   if (pi_camera_open(device))
      return -1;

   // Rotate image
   pi_camera_control(device, PI_CAMERA_CMD_START, 0);
   uint8_t set_value = 3;
   uint8_t reg_value;
   pi_camera_reg_set(device, IMG_ORIENTATION, &set_value);
   pi_time_wait_us(1000000);
   pi_camera_reg_get(device, IMG_ORIENTATION, &reg_value);
   if (set_value != reg_value) {
      cpxPrintToConsole(LOG_TO_CRTP, "Failed to rotate camera image\n");
      return -1;
   }
   pi_camera_control(device, PI_CAMERA_CMD_STOP, 0);
   pi_camera_control(device, PI_CAMERA_CMD_AEG_INIT, 0);

   return 0;
}

int main(void) {
   pi_bsp_init();

   return pmsis_kickoff((void *)start);
}

