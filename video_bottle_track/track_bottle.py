from ultralytics import YOLO
import cv2

model = YOLO("yolov8n.pt")

# Test avec camera locale
#results = model.track(source=0, show=True, tracker="bytetrack.yaml")

video_path = "./7_bottles.mp4"
cap = cv2.VideoCapture(video_path)
BOTTLE_ID = 39
ret = True
num_bottle = 0
while ret:
	ret, frame = cap.read()

	# End of video
	if frame is None:
		break

	results = model.track(frame, conf=0.1, persist=True)

	detected_object_ids = results[0].boxes.cls.cpu().numpy().tolist()
	for detected_object_id in detected_object_ids:
		if detected_object_id == BOTTLE_ID:
			if results[0].boxes.id != None:
				num_bottle = results[0].boxes.id.cpu().numpy().tolist()[0]

	frame_ = results[0].plot()

	cv2.imshow("frame", frame_)
	if cv2.waitKey(25) & 0xFF == ord('q'):
		break

print("There is "+str(num_bottle)+" bottle(s) in this video footage")