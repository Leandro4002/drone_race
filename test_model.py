import torch
import torch.nn as nn
from angle_finder import AngleFinder
import cv2
import numpy as np

model = AngleFinder()
model.load_state_dict(torch.load('followLineModel_edge.pth'))

def edge_detection(image_array):
    # Convert the image array to uint8
    image_uint8 = (image_array * 255).astype(np.uint8)
    
    # Apply Sobel edge detection
    edges_x = cv2.Sobel(image_uint8, cv2.CV_64F, 1, 0, ksize=3)
    edges_y = cv2.Sobel(image_uint8, cv2.CV_64F, 0, 1, ksize=3)

    # Combine the edge images
    edges = np.sqrt(edges_x**2 + edges_y**2)
    
    # Normalize the edges to [0, 1]
    edges /= edges.max()

    return edges

def detect_line(image):
   # A bit messy, but it's just a test
   image_np = np.array(image)
   image_np = edge_detection(image_np)
   image = image_np.tolist()

   image = torch.tensor(image)
   image = image.unsqueeze(1)
   pred = model(image)
   return pred

def read_and_convert_image(image_path, target_shape=(324, 244)):
   image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
   resized_image = cv2.resize(image, target_shape)
   float_image = [[float(pixel) for pixel in row] for row in resized_image]
   
   # Add a new axis to make it a 3D array with shape (1, height, width)
   float_image = [float_image]

   return float_image

images = []
images.append(read_and_convert_image("render/00_00.png"))
images.append(read_and_convert_image("render/00_10.png"))
images.append(read_and_convert_image("render/00_20.png"))
images.append(read_and_convert_image("render/00_30.png"))

preds = [detect_line(image) for image in images]

for pred in preds:
   print(pred[0][0].item())