#!/bin/bash

source gap_sdk/sdk_env/bin/activate
source gap_sdk/configs/ai_deck.sh

cd gap8
make build image flash
cd ..
