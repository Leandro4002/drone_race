<style>
    .center-img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>

<div style="text-align:center; font-size: 40px; margin-top: 400px">Labo IAA - Course de drone</div>
<div style="text-align:center; font-size: 40px; margin-top: 15px"></h1>
<div style="text-align:center; font-size: 18px; margin-top: 50px">11.06.2024</h3>
<div style="text-align:center; font-size: 18px; margin-top: 5px">Leandro SARAIVA MAIA</h3>
<div style="text-align:center; font-size: 18px; margin-top: 5px">Oscar BAUME</h3>
<div style="text-align:center; font-size: 18px; margin-top: 5px">Miguel JALUBE</h3>

<div style="page-break-after: always"></div>

# Introduction

Le but de ce projet est de faire suivre un drone le long d'une ligne et de calculer le nombre de bouteilles autour du circuit. Le circuit est dessiné à l'aide d'un scotch sur un drap noir.

Le Bitcraze AI deck 1.1 est monté sur un nanodrone Crazyflie 2.1. Le modèle de la caméra est Himax HM01B0.  
<img src="readme_assets/crazyflie_nanodrone_2.1.png" width="300"/>
<img src="readme_assets/bitcraze_aideck1.1.png" width="300"/>

Le circuit fait environ 3.5m x 1.8m, les images du drones sont monochrome, en 324x244 et les photos sont prises environs 15-30 centimètres au dessus du sol.

# Arborescence

Il y a beaucoup de fichiers, pas de panique !

```
|
|---blender_dataset_generation
|   "Dossier contentant le fichier blender et script python utilisé pour générer le dataset"
|
|---readme_assets
|   "Dossier contenant les images utilisées dans ce rapport"
|
|---render
|   "Dossier contenant toutes les images du dataset"
|
|---video_bottle_track
|   "Dossier contenant une video d'un tour du circuit (image du dataset) ainsi qu'un script
|   "python permettant de lancer le comptage du nombre de bouteille"
|
|---angle_finder.py
|   "Classe python représentant le modèle de deep learning"
|
|---flash_gap8.sh
|   "Script pour compiler le code du gap8 et pour le mettre sur le drone"
|
|---flash_stm32.sh
|   "Script pour compiler le code du stm32 et le mettre sur le drone"
|
|---followLineModel_edge.pth
|   "Poids pour le modèle de suivi de ligne avec un edge détection appliqué sur l'image."
|
|---followLineModel_raw.pth
|   "Poids pour le modèle de suivi de ligne ."
|
|---gap8.c
|   "Code du gap8"
|
|---generate_model.ipynb
|   "Notebook python permettant de générer le modèle de suivi de ligne"
|
|---labels.csv
|   "Label des données du dataset"
|
|---README.md
|   "Ce document au format markdown"
|
|---README.pdf
|   "Ce document au format pdf"
|
|---stm32.c
|   "Code du stm32"
|
|---test_model.py
    "Notebook python permettant de générer le modèle de suivi de ligne"
```

# Dataset

Nous utilisons le dataset LEANDRONE_V2 (https://huggingface.co/datasets/Leandro4002/LEANDRONE_V2) afin d'entraîner un modèle de deep learning pour que le robot sache où est-ce qu'il doit tourner.

A chaque itération (chaque tour autour du circuit), il ya des variations aléatoires. Le tracé de la ligne change et le nombre de bouteilles et leurs positions aussi.

Il y a 20 itérations de 40 images, ce qui donne un total de 800 images. A chaque nombre d'itération pair on fait un tour dans le sens des aiguilles d'une montre, et les itérations impaires on fait des tours dans le sens trigonométrique. Dans le nom de l'image, les 2 premiers numéros sont le numéro d'itération et les 2 prochains sont le numéro de l'image. Ils sont séparés par un '_'.

<div style="page-break-after: always"></div>

La caméra du GAP8 définit sa sensitivité à la lumière au démarrage. L'image capturé par la camera sera différent selon "l'exposure" de la caméra à la lumière.
Voici la comparaison lorsqu'on démarre le drone en cachant la caméra (covered) et lorsqu'on démarre en regardant une source lumineuse(blinded):  
<img src="readme_assets/sensi_salle.png" width=300/>
<img src="readme_assets/sensi_ligne.png" width=300/>  
Les images de ce dataset sont généré pour une caméra qui est couverte au démarrage. (Il est également possible de changer "l'exposure" dans le code avec des #defines).

Voici des comparaisons entre les images du dataset et des photos prisent avec le drone:  
<img src="readme_assets/comparison_dataset_actual.gif"/>  
Dans certaines images générées, il y a des points de lumière non voulus qui apparaissent sur l'image.

<div style="page-break-after: always"></div>

Un modèle 3D de la salle de test a été réalisé avec de la photogrammétrie et le logiciel Meshroom 2023.3.0:  
<img class="center-img" src="readme_assets/photogrammetry.png" width=500/>

Ensuite on importe ce modèle dans Blender 4.1 et on déplace la caméra le long de la ligne, on calcul l'angle entre la caméra et les prochains points afin d'obtenir le label et de générer l'image du point de vu de la caméra à cette endroit:  
<img class="center-img" src="readme_assets/blender_view_cropped.png" width=500/>

<div style="page-break-after: always"></div>

# Modèle pour suivre la ligne

La qualité n'est pas très bonne. Le modèle est très sensible aux perturbations de l'image. Lors des tests réels, nous avons constaté qu'il y a beaucoup de variabilité de valeur prédite. Les couleurs sur le logo d'une bouteille, une chaussure qui rentre dans le champ ou simplement le fait que les images du dataset ne correspondent pas tout à fait à la réalité font que le modèle retourne de mauvais résultats.

Nous avons essayé d'avoir plus d'images, de changer l'architecture du modèle et d'ajouter un edge detection, mais les performances du modèle sont toujours insuffisantes.

Voici quelques résultats du modèle. Une valeur positive tourne à droite, négative à gauche. (La partie grayscale remplace le blanc par du jaune et le noir par du bleu):  
<img src="readme_assets/results_raw.png" width=700/>
<img src="readme_assets/results_edge.png" width=700/>

<div style="page-break-after: always"></div>

# Comptage du nombre de bouteilles

Nous avons utilisé le modèle `yolov8n` afin de tracker les bouteilles. Ce modèle assigne un id au objet lorsqu'il est asset confiant d'avoir identifié l'objet. Pour tester le modèle, nous avons pris les images d'un tour de circuit de notre dataset et avons généré une vidéo. Sur une vidéo contenant 7 bouteilles, le modèle n'a pu en détecter que 2:  
<img src="readme_assets/bottle_1.png" width=200/>
<img src="readme_assets/bottle_2.png" width=200/>

Cela est probablement du au fait que ce modèle est entraîné avec des bouteilles ayant des formes standards, il y a parfois des boissons énergisantes et des thermos dans le dataset, cela a perturbé le modèle. De plus, la résolution de l'image n'est pas très grande et elle n'est que en monochrome. Autre chose, le moteur de rendu du modèle en 3D ne permet pas de gérer la transparence et affiche simplement du gris. On perd beaucoup de détail et le modèle n'arrive plus à détecter des bouteilles avec précision.

# Intégration sur le drone

En voyant tout les autres groupe échouer en utilisant un modèle deep learning et en constatant les nombreux problèmes inconsistants et aléatoire lié à l'utilisation du wifi, nous avons d'abandonner le modèle deep learning et de partir sur une stratégie de traitement d'image faite directement sur le drone. Le GAP8 fera le traitement de l'image et décidera d'où est-ce que le drone doit aller afin de suivre la ligne. Pour contrôler le drone, le GAP8 a un panel très restreint de commande, il envoie un seul byte. Afin que le système fonctionne le drone doit se déplacer très lentement afin de faire des micro ajustements en boucle.

Voici le déroulement de notre stratégie étapes par étapes :  
* Le GAP8 capture une image avec la caméra
* Le GAP8 met la partie supérieur de l'image en noir, il ne reste alors que la partie inférieur de l'image.
* Le GAP8 trouve le pixel qui est en moyenne le plus "lumineux" dans l'image. Pour ce faire il fait la moyenne de la valeur de chaque pixel par rapport à leur position. Comme la seul zone à peu près lumineuse est la ligne de scotch, le pixel trouvé sera à peu près au milieu de la bande de scotch. Si le pixel trouvé est à droite du milieu de l'image, il faudra que le drone tourne à droite et s'il est à gauche il devra tourner à gauche.
* Le GAP8 envoie un byte au STM32 qui servira à commander le drone: soit le drone ne bouge pas, soit il avance, soit il tourne à gauche, soit il tourne à droite.  
* Le STM32 reçoit la commande du GAP8 et l'applique en déplaçant le drone.
* On recommence ces étapes afin de suivre la ligne.

Malheureusement nous n'avons pas eu le temps de compléter notre algorithme. Il y a des TODO dans le code.

# Problèmes rencontrés

* Nous avons eu beaucoup de peine à simplement flasher le programme sur la GAP8 du drone. Lors du flash, le terminal plante et il ne se passe rien.
* Parfois, lors de la connexion avec le script python, on a le message "Connected to socket", mais ensuite il ne se passe rien et il est impossible de récupérer les images de la caméra du GAP8
* Lorsqu'on récupère les images du drones, on cesse de pouvoir recevoir des images au bout de 30 secondes. Ensuite on est obligé de redémarrer.

Pour chacun de ces problèmes, la seul manière de corriger ces erreurs et d'être sûr que cela fonctionne est de redémarrer la VM, redémarrer le drone et débrancher/rebrancher le drone. Il nous a été proposé de retirer/remettre le flow deck du drone mais cela ne fonctionnait pas.

Également, nous avons passé un long moment avec des pâles incorrectement placés dans notre drone et celui-ci refusait de redémarrer normalement. Il nous a fallu pas mal de temps avant de comprendre...

# Conclusion

Nous avons consacré la majeur partie de notre effort pour créer une dataset le plus fidèle à la réalité. Selon nous, c'est la partie la plus importante de ce projet car c'est elle qui va déterminer si on arrive oui ou non à suivre la ligne, le reste du travail devrait être plutôt simple car il "suffit" de récupérer les images capturés par le drone et de lui envoyer les commandes pertinentes. Malgré nos efforts, le modèle n'était toujours pas assez performant pour indiquer la bonne direction au drone.

C'est dommage que nous n'ayons pas pu faire fonctionner notre système sur le drone car c'est un labo qui pose un véritable challenge technique intéressant. Le hardware ne permettait pas d'itérer facilement et rendait le développement fastidieux. Toute la partie non liée au hardware formait une expérience plaisante, on a l'impression de résoudre un vrai problème et de ne pas seulement faire un exercice.
