#!/bin/bash

num=17

cd stm32
make -j$(nproc)
python3 -m cfloader flash build/cf2.bin stm32-fw -w "radio://0/80/2M/E7E7E7E7$num"
cd ..
