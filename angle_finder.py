import torch
import torch.nn as nn

class AngleFinder(nn.Module):
    def __init__(self):
        super(AngleFinder, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=4, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(in_channels=4, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.fc = nn.Linear(8 * 324 * 244, 1)  # Adjust input size based on image resolution
    
    def forward(self, x):
        x = nn.functional.relu(self.conv1(x))
        x = nn.functional.relu(self.conv2(x))
        x = torch.flatten(x, 1)  # Flatten the output of conv layer
        x = self.fc(x)
        return x